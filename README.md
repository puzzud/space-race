# space-race
Web based network multiplayer game proof of concept.

**Requirements**

- nodejs (Node JS)
- npm (Node Package Manager)

**Installation**

- Checkout this repository.
- Install **nodejs** and **npm**.
- From checkout **client** directory, issue the command:
```
npm install
```
- From checkout **server** directory, issue the command:
```
npm install
```

**Usage**

- From checkout **server** directory, issue the command:
```
npm start
```
- Open web browser to the following location:
http://localhost:8080
