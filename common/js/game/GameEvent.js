var GameEvent = {};

GameEvent.Type =
{
  START:        0,
  SPAWN:        1,
  ORIENT:       2,
  CONTROL:      3,
  COLLISION:    4,
  SCORE:        5,
  SHIP_ACTIVATE: 6
};
