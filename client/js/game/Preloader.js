/** @constructor */
App.Preloader = function(game)
{
  this.preloader = null;

  this.sounds = [];
  this.numberOfDecodedSounds = 0;
};

App.Preloader.stateKey = "Preloader";

App.Preloader.prototype.init = function()
{
  
};

App.Preloader.prototype.preload = function()
{
  var preloaderWidth = (this.game.width * 0.67 / 2.0) | 0;
  var preloaderHeight = 32;
  var bmd = this.game.add.bitmapData(preloaderWidth, preloaderHeight);
  bmd.ctx.fillStyle = "#999999";
  bmd.ctx.fillRect(0, 0, preloaderWidth, preloaderHeight);

  this.preloader = this.game.add.sprite(0, 0, bmd);
  this.preloader.anchor.setTo(0.5, 0.5);
  this.preloader.position.setTo(this.world.centerX,
                                 this.world.height - this.preloader.height * 2);
  this.load.setPreloadSprite(this.preloader);

  this.load.image("ship", "assets/graphics/ship.png");
  this.load.image("asteroid", "assets/graphics/asteroid.png");

  this.load.bitmapFont("font", "assets/fonts/pong.png", "assets/fonts/pong.fnt");

  this.load.audio("score", "assets/sounds/Score1.mp3");
  this.load.audio("hit", "assets/sounds/Hit1.mp3");
};

App.Preloader.prototype.create = function()
{
  this.numberOfDecodedSounds = 0;

  var scoreSound = this.game.add.audio("score");
  this.sounds.push(scoreSound);

  var hitSound = this.game.add.audio("hit");
  this.sounds.push(hitSound);

  if(this.sounds.length > 0)
  {
    this.sound.setDecodedCallback(this.sounds, this.allSoundsDecoded, this);
  }
  else
  {
    this.start();
  }
};

App.Preloader.prototype.soundDecoded = function(audio)
{
  // Start scaling the preloader sprite towards 200% for audio decoding.
  this.numberOfDecodedSounds++;
  this.preloader.scale.set(1.0 + (this.numberOfDecodedSounds / this.sounds.length), 1.0);
};

App.Preloader.prototype.allSoundsDecoded = function()
{
  this.start();
};

App.Preloader.prototype.start = function()
{
  this.state.start("game");
};
