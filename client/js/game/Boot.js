/** @constructor */
App.Boot = function(game)
{
  
};

App.Boot.prototype.init = function()
{
  this.stage.disableVisibilityChange = true;
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.scale.minWidth = (320 / 2) | 0;
  this.scale.minHeight = (200 / 2) | 0;
  this.scale.pageAlignHorizontally = true;
  this.scale.pageAlignVertically = true;
  this.stage.forcePortrait = true;

  this.input.maxPointers = 1;
  this.input.addPointer();

  this.stage.backgroundColor = 0x000000;
  
  App.client = new WS.Client();
};

App.Boot.prototype.preload = function()
{
  
};

App.Boot.prototype.create = function()
{
  this.state.start("preloader");
};
