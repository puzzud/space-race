/** @constructor */
App =
{
  game: null,
  
  client: null
};

App.run = function()
{
  this.game = new Phaser.Game(320, 200, Phaser.AUTO, "", this, false, false);

  this.game.state.add("boot", App.Boot);
  this.game.state.add("preloader", App.Preloader);
  this.game.state.add("game", App.Game);

  this.game.state.start("boot");
};
