/** @constructor */
App.Game = function(game)
{
  this.scores = [];

  this.spaceBar = null;
  
  this.backgroundSprite = null;

  this.scoreText = [];

  this.entities = [];

  this.ships = [];
  this.shipVelocities = [];
  this.shipActive = [];

  this.asteroids = [];

  this.scoreSound = null;
  this.hitSound = null;
};

App.Game.prototype.init = function()
{
  
};

App.Game.prototype.create = function()
{
  this.setupGraphics();
  this.setupInput();
  this.setupNetwork();
  this.setupAudio();

  var content =
  {
    type: GameEvent.Type.ORIENT
  };
  
  App.client.request(content);

  var content =
  {
    type: GameEvent.Type.SCORE
  };
  
  App.client.request(content);

  var content =
  {
    type: GameEvent.Type.SHIP_ACTIVATE
  };
  
  App.client.request(content);
};

App.Game.prototype.setupInput = function()
{
  this.spaceBar = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  this.spaceBar.onDown.add(this.onSpaceBarDown, this);
  this.spaceBar.onUp.add(this.onSpaceBarUp, this);
  
  this.backgroundSprite.inputEnabled = true;
  this.backgroundSprite.input.priorityID = 0;
  this.backgroundSprite.events.onInputDown.add(this.onPointerDown, this);
  this.backgroundSprite.events.onInputUp.add(this.onPointerUp, this);
};

App.Game.prototype.setupGraphics = function()
{
  this.setupBackground();

  this.setupShips();
  this.setupAsteroids();

  this.game.stage.smoothed = false;

  this.game.renderer.renderSession.roundPixels = true;
  Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);

  //this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
  //this.game.scale.setUserScale(4, 4);

  this.scoreText.push(this.game.add.bitmapText(32, 8, "font", "0", 32));
  this.scoreText.push(this.game.add.bitmapText(320 - 64 + 16, 8, "font", "0", 32));

  this.scoreText[0].tint = 0xaaaaaa;
  this.scoreText[1].tint = 0xaaaaaa;
};

App.Game.prototype.setupNetwork = function()
{
  App.client.onUpdate = this.onNetworkUpdate.bind(this);
};

App.Game.prototype.setupAudio = function()
{
  this.scoreSound = this.game.add.audio("score");
  this.hitSound = this.game.add.audio("hit");
};

App.Game.prototype.setupBackground = function()
{
  this.stage.backgroundColor = 0x222222;
  
  this.backgroundSprite = this.game.add.sprite(0, 0);
  this.backgroundSprite.fixedToCamera = true;
  this.backgroundSprite.scale.setTo(this.game.width, this.game.height);

  this.game.world.sendToBack(this.backgroundSprite);
};

App.Game.prototype.setupShips = function()
{
  this.ships.push(this.add.sprite(0, 0, "ship"));
  this.ships.push(this.add.sprite(0, 0, "ship"));

  this.entities = this.entities.concat(this.ships);
  
  var ship = null;

  for(var i = 0; i < this.ships.length; ++i)
  {
    ship = this.ships[i];

    ship.texture.baseTexture.scaleMode = Phaser.scaleModes.NEAREST;

    this.shipVelocities.push(0.0);

    ship.anchor.x = ship.anchor.y = 0.5;

    this.shipActive.push(false);

    this.updateShipColor(i);
  }
};

App.Game.prototype.updateShipColor = function(shipIndex)
{
  var ship = this.ships[shipIndex];
  
  if(this.shipActive[shipIndex])
  {
    ship.tint = (shipIndex === 0) ? 0xaa4444 : 0x4444aa;
  }
  else
  {
    ship.tint = 0x444444;
  }
};

App.Game.prototype.setupAsteroids = function()
{
  var asteroid = null;

  for(var i = 0; i < 5; ++i)
  {
    asteroid = this.add.sprite(0, 0, "asteroid");
    this.asteroids.push(asteroid);

    asteroid.tint = 0xaaaaaa;

    asteroid.texture.baseTexture.scaleMode = Phaser.scaleModes.NEAREST;

    //asteroid.scale.x = 2;
    //asteroid.scale.y = 2;

    asteroid.anchor.x = asteroid.anchor.y = 0.5;
  }

  this.entities = this.entities.concat(this.asteroids);
};

App.Game.prototype.update = function()
{
  this.moveShips();
  this.moveAsteroids();
};

App.Game.prototype.moveShips = function()
{
  var ship = null;
  var shipVelocity = 0.0;

  for(var i = 0; i < this.ships.length; ++i)
  {
    shipVelocity = this.shipVelocities[i];

    if(shipVelocity > 0.0)
    {
      ship = this.ships[i];
      ship.y -= shipVelocity;

      if(ship.y < 0)
      {
        this.resetShip(i);

        this.scoreSound.play();
        this.scoreSound._sound.playbackRate.value = 1.0 - ((1 / 12) * i * 2);
      }
    }
  }
};

App.Game.prototype.positionEntities = function(positions)
{
  var position = null;
  var entity = null;

  for(var i = 0; i < this.entities.length; ++i)
  { 
    entity = this.entities[i];

    position = positions[i];

    if(entity === undefined || position === undefined)
    {
      return;
    }

    entity.x = position.x;
    entity.y = position.y;
  }
};

App.Game.prototype.moveAsteroids = function()
{
  var asteroid = null;
  var asteroidVelocity = 0.0;

  for(var i = 0; i < this.asteroids.length; ++i)
  {
    asteroidVelocity = 0.9 * (((i & 1) === 0) ? -1.0 : 1.0);

    asteroid = this.asteroids[i];
    asteroid.x += asteroidVelocity;

    if(asteroidVelocity < 0.0 && asteroid.x < (0 - 8))
    {
      asteroid.x = 320;
    }
    else if(asteroidVelocity > 0.0 && asteroid.x > 320)
    {
      asteroid.x = 0 - 8;
    }
  }
};

App.Game.prototype.resetShip = function(shipIndex)
{
  var ship = this.ships[shipIndex];
  ship.y = 200 - 10;
};

App.Game.prototype.setScore = function(playerId, score)
{
  var shipIndex = playerId % 2;

  this.scores[shipIndex] = score;

  this.scoreText[shipIndex].text = score;

  if(shipIndex === 1)
  {
    var numberOfDigits = (score + "").length;
    
    this.scoreText[1].x = 320 - 64 + 16 - ((numberOfDigits - 1) * 32);
  }
};

App.Game.prototype.onSpaceBarDown = function()
{
  this.onControllerDown();
};

App.Game.prototype.onSpaceBarUp = function()
{
  this.onControllerUp();
};

App.Game.prototype.onPointerDown = function(sprite, pointer)
{
  this.onControllerDown();
};

App.Game.prototype.onPointerUp = function(sprite, pointer)
{
  this.onControllerUp();
};

App.Game.prototype.onControllerDown = function()
{
  var content =
  {
    type: GameEvent.Type.CONTROL,
    on: true
  };
  
  App.client.request(content);
};

App.Game.prototype.onControllerUp = function()
{
  var content =
  {
    type: GameEvent.Type.CONTROL,
    on: false
  };
  
  App.client.request(content);
};

App.Game.prototype.onNetworkUpdate = function(content)
{
  var type = content.type;

  if(type === GameEvent.Type.CONTROL)
  {
    var on = content.on;

    var playerId = content.playerId;
    var shipIndex = playerId % 2;

    this.shipVelocities[shipIndex] = (on ? 1.0 : 0.0) * 1.0;

    return;
  }

  if(type === GameEvent.Type.ORIENT)
  {
    this.positionEntities(content.positions);

    return;
  }

  if(type === GameEvent.Type.COLLISION)
  {
    var shipIndex = content.shipIndex;

    this.resetShip(shipIndex);
    this.hitSound.play();

    return;
  }

  if(type === GameEvent.Type.SCORE)
  {
    var playerId = content.playerId;

    this.setScore(playerId, content.score);

    return;
  }

  if(type === GameEvent.Type.SHIP_ACTIVATE)
  {
    var playerId = content.playerId;
    var active = content.active;

    var shipIndex = playerId % 2;

    this.shipActive[shipIndex] = active;
    this.updateShipColor(shipIndex);

    return;
  }
};
