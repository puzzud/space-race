var WS = {};

WS.Client = function()
{
  var address = "";//"localhost:8080";
  address = window.location.href;
  address = address.replace("https", "ws");
  address = address.replace("http", "ws");
  //address += "ws";
  
  this.ws = new WebSocket(address);
  this.ws.onload = this.onload.bind(this);
  this.ws.onmessage = this.onmessage.bind(this);
  this.ws.onclose = this.onclose.bind(this);

  this.verbose = false;

  // Set up package processor table.
  this.packageProcessors = [];
  this.packageProcessors[protocol.PackageType.MESSAGE] = WS.Client.prototype.processMessagePackage;
  this.packageProcessors[protocol.PackageType.JOIN] = WS.Client.prototype.processJoinPackage;
  this.packageProcessors[protocol.PackageType.UPDATE] = WS.Client.prototype.processUpdatePackage;

  // Custom package processor hooks.
  this.onUpdate = undefined;
};

WS.Client.prototype.onload = function()
{
  console.log("Client loaded.");
};

WS.Client.prototype.onmessage = function(event)
{
  this.processPackage(JSON.parse(event.data));
};
    
WS.Client.prototype.onclose = function()
{
  console.log("Closed connection with server.");
};

WS.Client.prototype.processPackage = function(package)
{
  var packageProcessor = this.packageProcessors[package.type];
  
  if(packageProcessor === undefined)
  {
    console.error("WS.Client::processPackage does not have a processor for a received package type " + package.type + ".");

    return;
  }

  packageProcessor.call(this, package);
};

WS.Client.prototype.sendPackage = function(package)
{
  this.ws.send(JSON.stringify(package));
};

WS.Client.prototype.request = function(content)
{
  if(this.verbose)
  {
    console.log("Requesting.");
  }

  var package = protocol.createRequestPackage(content);
  
  this.sendPackage(package);
};

WS.Client.prototype.processMessagePackage = function(package)
{
  if(this.verbose)
  {
    console.log(package.message);
  }
};

WS.Client.prototype.processJoinPackage = function(package)
{
  if(this.verbose)
  {
    var clientId = package.content.clientId;

    var message = "Client " + clientId + " connected.";

    console.log(message);
  }
};

WS.Client.prototype.processUpdatePackage = function(package)
{
  if(this.verbose)
  {
    console.log("Recieved update.");
  }
  
  if(this.onUpdate !== undefined)
  {
    this.onUpdate(package.content);
  }
};
