// server
// Implemented using:
// https://github.com/websockets/ws

(
  function(exports)
  { 
    var rootPath = __dirname + "/../../..";
    var commonPath = rootPath + "/common";

    var protocol = require(commonPath + "/js/core/Protocol");

    exports.WsServer = function()
    {
      this.connectedClients = [];
      this.clientIdCounter = 0;

      WebSocket = require("ws"); // NOTE: Purposely global.
      
      this.express = require("express");
      this.app = this.express();
      var http = require("http");
      this.server = http.Server(this.app);
      
      var server = this.server;
      
      this.wss = new WebSocket.Server({server});
      this.wss.on("connection", this.onconnection.bind(this));
      
      this.verbose = false;
      
      // Set up package processor table.
      this.packageProcessors = [];
      this.packageProcessors[protocol.PackageType.MESSAGE] = exports.WsServer.prototype.processMessagePackage;
      this.packageProcessors[protocol.PackageType.REQUEST] = exports.WsServer.prototype.processRequestPackage;
      
      // Custom package processor hooks.
      this.onClientJoin = undefined;
      this.onRequest = undefined;
    };

    exports.WsServer.prototype.start = function()
    {
      // Set up static website.
      var path = require("path");
      
      var clientPath = path.resolve(rootPath + "/client");
      var indexHtmlPath = path.resolve(clientPath + "/index.html");

      this.app.get
      (
        "/",
        function(req, res)
        { 
          res.sendFile(indexHtmlPath);
        }
      );

      var jsPath = path.resolve(clientPath + "/js");
      this.app.use("/js", this.express.static(jsPath));

      var nodeModulesPath = path.resolve(clientPath + "/node_modules");
      this.app.use("/node_modules", this.express.static(nodeModulesPath));

      this.app.use("/common", this.express.static(path.resolve(commonPath)));

      var cssPath = path.resolve(clientPath + "/css");
      this.app.use("/css", this.express.static(cssPath));

      var assetsPath = path.resolve(clientPath + "/assets");
      this.app.use("/assets", this.express.static(assetsPath));

      var port = 8080;

      if(process.argv.length > 2)
      {
        port = parseInt(process.argv[2]);
      }

      this.server.listen(port);
    };

    exports.WsServer.prototype.onconnection = function(ws)
    {
      var wsSocket = new exports.WsSocket(ws, this);
      
      var clientId = wsSocket.clientId;
      
      console.log("Client " + clientId + " connected.");
      
      var content =
      {
        clientId: clientId
      };
      
      var package = protocol.createJoinPackage(content);
      this.broadcastPackage(package, wsSocket);
      
      if(this.onClientJoin !== undefined)
      {
        this.onClientJoin(content, wsSocket);
      }
    };

    exports.WsServer.prototype.sendPackage = function(ws, package)
    {
      ws.send(JSON.stringify(package));
    };

    exports.WsServer.prototype.broadcastPackage = function(package, allBut)
    {
      allBut = (allBut === undefined) ? null : allBut.ws;
      
      var packageString = JSON.stringify(package);
      
      this.wss.clients.forEach
      (
        function each(client)
        {
          if(client !== allBut)
          {
            if(client.readyState === WebSocket.OPEN)
            {
              client.send(packageString);
            }
          }
        }
      );
    };

    exports.WsServer.prototype.processPackage = function(package, wsSocket)
    {
      var packageProcessor = this.packageProcessors[package.type];
      
      if(packageProcessor === undefined)
      {
        this.sendPackage(wsSocket.ws, protocol.createErrorPackage("unknown message type"));
        
        return;
      }

      packageProcessor.call(this, package, wsSocket);
    };

    exports.WsServer.prototype.processMessagePackage = function(package, wsSocket)
    {
      this.broadcastPackage(protocol.createErrorPackage(package.message));
    };

    exports.WsServer.prototype.processRequestPackage = function(package, wsSocket)
    {
      if(this.verbose)
      {
        console.log("Received request from client " + wsSocket.clientId + ".");
      }
      
      if(this.onRequest !== undefined)
      {
        this.onRequest(package.content, wsSocket);
      }
    };
    
    exports.WsServer.prototype.sendUpdate = function(content, wsSocket)
    {
      var clientId = wsSocket.clientId;
      
      this.sendPackage(wsSocket.ws, protocol.createUpdatePackage(content));
      
      if(this.verbose)
      {
        console.log("Sending update to client " + clientId + ".");
      }
    };
    
    exports.WsServer.prototype.broadcastUpdate = function(content, wsSocket)
    {
      this.broadcastPackage(protocol.createUpdatePackage(content));
      
      if(this.verbose)
      {
        console.log("Sending update to all clients.");
      }
    };

    exports.WsSocket = function(ws, wsServer)
    {
      this.ws = ws;
      this.wsServer = wsServer;
      this.clientId = ++wsServer.clientIdCounter;
      
      ws.on("message", this.onmessage.bind(this));
      ws.on("close", this.onclose.bind(this));
      
      wsServer.connectedClients.push(this);
      
      console.log("Sending connection confirmation to client " + this.clientId + ".");
      
      var package = protocol.createMessagePackage("Connected as client " + this.clientId + ".");
      wsServer.sendPackage(ws, package);
    };

    exports.WsSocket.prototype.onmessage = function(data)
    {
      var package = JSON.parse(data);
      this.wsServer.processPackage(package, this);
    };

    exports.WsSocket.prototype.onclose = function()
    {
      var wsServer = this.wsServer;
      var index = wsServer.connectedClients.indexOf(this);
      if(index > -1)
      {
        wsServer.connectedClients = wsServer.connectedClients.splice(index, 1);
        
        var message = "Client " + this.clientId + " disconnected.";
        console.log(message);
        
        var package = protocol.createMessagePackage(message);
        wsServer.broadcastPackage(package, this);
      }
    };}
)(typeof exports === "undefined" ? this["server"] = {}: exports);
{}
