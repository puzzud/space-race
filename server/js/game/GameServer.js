var fs = require("fs");
eval(fs.readFileSync(__dirname + "/../../../common/js/game/GameEvent.js") + "");

var GameServer = function()
{
  server = require(__dirname + "/../core/Server");
  this.wsServer = new server.WsServer();
  
  this.wsServer.onClientJoin = this.onClientJoin.bind(this);
  this.wsServer.onRequest = this.onRequest.bind(this);
  
  this.scores = [0, 0];
  
  this.entities = [];
  this.ships = [];
  this.shipVelocities = [];
  this.shipActive = [];
  this.asteroids = [];
  
  this.setupEntities();
  
  this.wsServer.start();
  
  this.lastTime = new Date();
  setInterval(this.update.bind(this), 16.667);
  
  setInterval(this.updateClients.bind(this), 100);
};

// Orient all entities.
GameServer.prototype.orientAll = function()
{
  var content =
  {
    type: GameEvent.Type.ORIENT,
    positions: this.entities
  };
  
  this.wsServer.broadcastUpdate(content);
};

GameServer.prototype.onRequest = function(content, wsSocket)
{
  var type = content.type;
  
  if(type === GameEvent.Type.ORIENT)
  {
    this.orientAll();
    
    return;
  }
  
  if(type === GameEvent.Type.CONTROL)
  {
    var playerId = wsSocket.clientId;
    var on = content.on;
    
    var content =
    {
      type: GameEvent.Type.CONTROL,
      playerId: playerId,
      on: on
    };
    
    this.wsServer.broadcastUpdate(content);
    
    var shipIndex = playerId % 2;
    this.shipVelocities[shipIndex] = (on ? 1.0 : 0.0) * 60.0;
    
    return;
  }
  
  if(type === GameEvent.Type.SCORE)
  {
    for(var i = 0; i < this.ships.length; ++i)
    {
      var playerId = i % 2;
      
      var content =
      {
        type: GameEvent.Type.SCORE,
        playerId: playerId,
        score: this.scores[playerId]
      };
      
      this.wsServer.broadcastUpdate(content);
    }
    
    return;
  }
  
  if(type === GameEvent.Type.SHIP_ACTIVATE)
  {
    for(var i = 0; i < this.ships.length; ++i)
    {
      var playerId = i % 2;
      
      var content =
      {
        type: GameEvent.Type.SHIP_ACTIVATE,
        playerId: playerId,
        active: this.shipActive[playerId]
      };
      
      this.wsServer.broadcastUpdate(content);
    }
    
    return;
  }
};

GameServer.prototype.onClientJoin = function(content, wsSocket)
{
  this.orientAll();
  
  var playerId = wsSocket.clientId;
  
  var shipIndex = playerId % 2;
  this.shipActive[shipIndex] = true;
};

GameServer.prototype.setupEntities = function()
{
  this.ships =
  [
    {x: ((320 / 2) | 0) - 64, y: 200 - 10},
    {x: ((320 / 2) | 0) + 64, y: 200 - 10}
  ];
  
  this.shipVelocities.push(0.0);
  this.shipVelocities.push(0.0);
  
  this.shipActive.push(false);
  this.shipActive.push(false);
  
  this.asteroids =
  [
    {x: 0, y: 32},
    {x: 320 - 32, y: 64},
    {x: 64, y: 96},
    {x: 320 - 96, y: 128},
    {x: 128, y: 160}
  ];
  
  this.entities = this.ships.concat(this.asteroids);
};

GameServer.prototype.update = function()
{
  var now = new Date();
  var dt = now - this.lastTime;
  dt /= 1000.0;
  this.lastTime = now;
  
  this.moveShips(dt);
  this.moveAsteroids(dt);
  
  this.checkCollisions();
};

GameServer.prototype.updateClients = function()
{
  this.orientAll();
};

GameServer.prototype.moveShips = function(dt)
{
  var ship = null;
  var shipVelocity = 0.0;

  for(var i = 0; i < this.ships.length; ++i)
  {
    shipVelocity = this.shipVelocities[i];

    if(shipVelocity > 0.0)
    {
      ship = this.ships[i];
      ship.y -= dt * shipVelocity;

      if(ship.y < 0)
      {
        this.resetShip(i);
        
        this.setScore(i, ++this.scores[i]);
      }
    }
  }
};

GameServer.prototype.moveAsteroids = function(dt)
{
  var asteroid = null;
  var asteroidVelocity = 0.0;

  for(var i = 0; i < this.asteroids.length; ++i)
  {
    asteroidVelocity = dt * 0.9 * (((i & 1) === 0) ? -60.0 : 60.0);

    asteroid = this.asteroids[i];
    asteroid.x += asteroidVelocity;

    if(asteroidVelocity < 0.0 && asteroid.x < (0 - 8))
    {
      asteroid.x = 320;
    }
    else if(asteroidVelocity > 0.0 && asteroid.x > 320)
    {
      asteroid.x = 0 - 8;
    }
  }
};

GameServer.prototype.checkCollisions = function()
{
  var asteroid = null;
  var ship = null;
  
  var asteroidIndex = 0;
  var shipIndex = 0;

  for(shipIndex = 0; shipIndex < this.ships.length; ++shipIndex)
  {
    ship = this.ships[shipIndex];
    
    for(asteroidIndex = 0; asteroidIndex < this.asteroids.length; ++asteroidIndex)
    {
      asteroid = this.asteroids[asteroidIndex];
      
      if(this.getDistanceBetweenEntities(ship, asteroid) <= 10.0)
      {
        this.handleCollision(shipIndex);
        
        break;
      }
    }
  }
};

GameServer.prototype.getDistanceBetweenEntities = function(entity1, entity2)
{
  var dx = entity2.x - entity1.x;
  var dy = entity2.y - entity1.y;
  
  return Math.sqrt((dx * dx) + (dy * dy));
};

GameServer.prototype.handleCollision = function(shipIndex)
{
  this.resetShip(shipIndex);
  
  var content =
  {
    type: GameEvent.Type.COLLISION,
    shipIndex: shipIndex
  };
  
  this.wsServer.broadcastUpdate(content);
};

GameServer.prototype.resetShip = function(shipIndex)
{
  var ship = this.ships[shipIndex];
  ship.y = 200 - 10;
};

GameServer.prototype.setScore = function(playerId, score)
{
  playerId = playerId % 2;
  
  this.scores[playerId] = score;
  
  var content =
  {
    type: GameEvent.Type.SCORE,
    playerId: playerId,
    score: score
  };
  
  this.wsServer.broadcastUpdate(content);
};


new GameServer();
